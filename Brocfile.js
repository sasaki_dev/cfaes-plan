/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app'),
    pickFiles = require('broccoli-static-compiler'),
    mergeTrees = require('broccoli-merge-trees');

var app = new EmberApp();

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.


// Bootstrap

app.import('bower_components/bootstrap/dist/css/bootstrap.css');
app.import('bower_components/bootstrap/dist/js/bootstrap.js');

// gives a 404 if we use dist/bootstrap.css
var bootstrapMap = pickFiles('bower_components/bootstrap/dist/css', {
    srcDir: '/',
    files: ['bootstrap.css.map'],
    destDir: '/assets'
});

var bootstrapFonts = pickFiles('bower_components/bootstrap/dist/fonts', {
  srcDir: '/',
  destDir: '/fonts/'
});


// Markdown
app.import('bower_components/markdown/lib/markdown.js');


// D3 and colorbrewer
app.import('bower_components/d3/d3.js');


// Typeahead
app.import('bower_components/typeahead.js/dist/typeahead.jquery.js');


// Merge everything
module.exports = mergeTrees([
  app.toTree(),
  bootstrapMap,
  bootstrapFonts
], { overwrite: true });
