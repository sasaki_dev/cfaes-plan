/* global module, process */

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
      tmp: ['./.tmp'],
      // images and pdfs are managed "manually" outside of grunt
      dist: ['./dist/images/chapter-*.jpg', './dist/images/page-*.jpg', './dist/pdf']
    },

    copy: {
      compressed: {
        files: [
        { expand: true, cwd: './.tmp/compress/dist/', src: ['**'], dest: './dist/'}
        ]
      }
    },

    compress: {
      options: {
        mode: 'gzip',
      },
      dist: {
        expand: true,
        cwd: './dist/',
        src: ['**/*.{css,js,html}'],
        dest: './.tmp/compress/dist/',
        filter: 'isFile',
        // keep the original .ext(ension) rather than *.ext.gz
        ext: function (ext) { return ext; }
      }
    },

    aws_s3: {
      options: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        region: process.env.AWS_REGION,
        access: 'public-read',
        differential: true
      },
      staging: {
        options: {
          bucket: 'cfaes-plan.development.sasaki.com',
          params: {
            // no cache
            CacheControl: "max-age=0",
            Expires: new Date(Date.now())
          }
        },
        files: [{
          expand: true,
          cwd: 'dist',
          src: ['**'],
          dest: ''
        }]
      },
      production: {
        options: {
          bucket: 'cfaes-plan.sasaki.com',
          differential: true,
          params: {
            // 1 month cache policy (1000 * 60 * 60 * 24 * 30)
            CacheControl: "max-age=2592000000, public",
            Expires: new Date(Date.now() + 2592000000)
          }
        },
        files: [
        {
          expand: true,
          cwd: 'dist',
          src: [
          '**',
          '!*.html',
          '!**/*.{js,css}'
          ],
          dest: ''
        }, {
          expand: true,
          cwd: 'dist',
          src: [
          '**/*.html',
          '**/*.{js,css}'
          ],
          dest: '',
          params: {
            ContentEncoding: 'gzip'
          }
        }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-aws-s3');

  grunt.registerTask('deploy', [
    'clean:tmp',
    'clean:dist',
    'compress:dist',
    'copy:compressed',
    'aws_s3:production',
    'clean:tmp'
  ]);

  grunt.registerTask('deploy:staging', [
    'clean:dist',
    'aws_s3:staging'
  ]);

};
