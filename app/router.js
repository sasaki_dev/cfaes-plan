import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {

  this.resource('chapters', { path: '/' }, function () {

    this.resource('chapter', { path: '/chapters/:chapter_id' });
    this.resource('page', { path: '/pages/:page_id'});

  });

  this.resource('chart', { path: '/charts/:chart_id' } );

  this.route('support-docs');

});

export default Router;
