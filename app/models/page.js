import DS from 'ember-data';

export default DS.Model.extend({

  nr:       DS.attr('number'),
  body:     DS.attr('string'),

  chapter:  DS.belongsTo('chapter', { async: true })

});
