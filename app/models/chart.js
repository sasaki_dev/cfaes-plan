import DS from 'ember-data';

export default DS.Model.extend({

  title:        DS.attr('string'),
  nr:           DS.attr('number'),
  description:  DS.attr('string'),
  thumbnail:    DS.attr('string'),
  ember_view:   DS.attr('string'),
  graph_data:   DS.attr()

});
