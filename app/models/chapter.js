import DS from 'ember-data';

export default DS.Model.extend({

  title:      DS.attr('string'),
  slug:       DS.attr('string'),
  nr:         DS.attr('number'),

  thumbnail:  DS.attr('string'),

  pages:      DS.hasMany('page', { async: true })

});
