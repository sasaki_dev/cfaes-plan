import Ember from 'ember';

export default Ember.Controller.extend({

  toPrint: [],
  toPrintSorting: ['nr'],
  toPrintSorted: Ember.computed.sort('toPrint', 'toPrintSorting'),
  isCustomReport: Ember.computed.notEmpty('toPrint'),

  printBtnText: function () {

    if (this.get('isCustomReport')) {

      if (this.get('toPrint.length') === 1) {
        return 'Download Chapter ' + this.get('toPrint.firstObject.nr');
      } else {
        return 'Download custom report';
      }

    } else {

      return 'Download Report';

    }

  }.property('toPrint.@each'),


  isPreparingPdf: false,
  pdfUrl: null,
  customPdfError: false,
  customPdfErrorMsg: null,

  actions: {

    downloadCustomReport: function () {
      var controller = this,
          chapters;

      this.set('isPreparingPdf', true);
      this.set('customPdfError', false);
      chapters = this.get('toPrintSorted').mapBy('nr');

      // add report cover
      chapters.unshift('cover');

      // Call PDF-generation endpoint
      Ember.$.ajax({
        url: '/api/pdf',
        data: { chapters: chapters }
      })
      .fail(function(xhr) {
        controller.set('customPdfError', true);
        controller.set('customPdfErrorMsg', xhr.responseText);
        controller.set('isPreparingPdf', false);
      })
      .done(function (data) {
        controller.set('isPreparingPdf', false);
        controller.set('pdfUrl', data.url);
      });

      // remove report cover
      chapters.shift();

      Ember.$('#downloadModal').modal('show');

    }

  }

});
