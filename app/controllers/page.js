import Ember from 'ember';

export default Ember.Controller.extend({

  needs: ['application', 'chapters'],


  toPrint: Ember.computed.alias('controllers.application.toPrint'),
  updateToPrint: function () {

    this.set('printBtnText', 'Download this chapter');
    this.set('toPrint', [this.get('model.chapter')]);

  }.observes('model.chapter'),


  sorting: ['nr'],

  chapters: Ember.computed.alias('controllers.chapters.content'),
  sortedChapters: Ember.computed.sort('chapters', 'sorting'),


  // hard coded image url
  imageUrl: function () {
    return '/images/page-' + this.get('model.id') + '.jpg';
  }.property('model'),


  allPages: function () {
    return this.get('store').find('page').then(function (pages) {
      return pages.sortBy('nr');
    });
  }.property(),

  previousPage: null,
  nextPage: null,

  updatePageNav: function () {

    this.get('allPages').then(function (pages) {
      var idx, prev, next;

      idx = pages.indexOf(this.get('model'));
      prev = pages.objectAt(idx - 1);
      next = pages.objectAt(idx + 1);

      this.set('previousPage', prev);
      this.set('nextPage', next);

    }.bind(this));

  }.observes('model'),


  previousChapter: null,
  nextChapter: null,

  updateChapterNav: function () {

    this.get('model.chapter').then(function (chapter) {
      var idx, prev, next;

      idx = this.get('sortedChapters').indexOf(chapter);
      prev = this.get('sortedChapters').objectAt(idx - 1);
      next = this.get('sortedChapters').objectAt(idx + 1);

      this.set('previousChapter', prev);
      this.set('nextChapter', next);

    }.bind(this));

  }.observes('model.chapter')

});
