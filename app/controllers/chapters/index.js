import Ember from 'ember';

export default Ember.ArrayController.extend({

  needs: ['application'],
  sortProperties: ['nr'],

  chaptersPerRow: 4,


  toPrint: Ember.computed.alias('controllers.application.toPrint'),

  gridRows: function () {
    var rows = [],
        newRow = [],
        threshold = this.get('chaptersPerRow');

    this.get('arrangedContent').sortBy('nr').forEach(function (chapter, i, coll) {
      // start a new row on every fourth item
      if (i%threshold === 0 && newRow.length) {
        rows.push(newRow);
        newRow = [];
      }
      if (i+1 === coll.length) { rows.push(newRow); } // last row, with remaning items
      newRow.push(chapter);
    });

    return rows;

  }.property('content')

});
