import Ember from 'ember';

export default Ember.Route.extend({

  model: function(params) {
    return this.store.find('chapter', params.chapter_id);
  },

  afterModel: function(model) {
    var route = this;

    model.get('pages').then(function (pages) {
      if (pages.get('length')) {
        route.transitionTo('page', pages.sortBy('nr').get('firstObject'));
      }
    });
    
  }

});
