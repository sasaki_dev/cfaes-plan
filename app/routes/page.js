import Ember from 'ember';

export default Ember.Route.extend({

  model: function(params) {
    return this.store.find('page', params.page_id);
  },

  actions: {

    willTransition: function () {
      // empty global print array, better UX
      this.controllerFor('application').get('toPrint').clear();

    }

  }

});
