import Ember from 'ember';

export default Ember.Route.extend({

  model: function(params) {
    return this.store.find('chart', params.chart_id);
  }

});
