import Ember from 'ember';

export default Ember.Component.extend({

  classNames: ['chapter-box'],
  classNameBindings: ['isSelected:selected'],

  isSelected: false,

  tooltipText: function () {

    if (this.get('isSelected')) {
      return 'Remove from custom report';
    }
    return 'Add to custom report';

  }.property('isSelected'),

  updateToPrint: function () {
    if (this.get('isSelected')) {
      this.get('toPrint').addObject(this.get('chapter'));
    } else {
      this.get('toPrint').removeObject(this.get('chapter'));
    }
  }.observes('isSelected'),

  didInsertElement: function() {

    this.$('.download-icon').on({
      mouseover: function () {
        this.$('.tooltip').css('opacity', 1);
      }.bind(this),
      mouseout: function () {
        this.$('.tooltip').css('opacity', 0);
      }.bind(this)
    });

  },

  actions: {
    selectChapter: function() {
      this.toggleProperty('isSelected');
    }
  }

});
