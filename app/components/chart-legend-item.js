import Ember from 'ember';

export default Ember.Component.extend({

  classNames: ['legend-item'],

  didInsertElement: function() {

    var cmp = this;

    var svgContainer = d3.select(cmp.$()[0])
      .insert('svg', ':first-child')
      .attr('width', 30)
      .attr('height', 30);

    svgContainer
      .append('circle')
      .attr('cx', 15)
      .attr('cy', 15)
      .attr('r', 10)
      .attr('stroke-width', 1.4)
      .attr('stroke', 'white')
      .attr('fill', cmp.get('color'));

  }

});
