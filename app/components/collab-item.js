import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',

  item: null,
  hoveredNode: null,

  attributeBindings: ['style'],
  style: function() {
    var color = this.get('item.color') || 'rgb(255,255,255)';

    // transparency
    color = color.substr(0, color.length - 1) + ', 0.4)';
    color = color.replace('rgb', 'rgba');

    return 'background-color: ' + color + ';';
  }.property('color'),

  mouseEnter: function() {
    this.set('hoveredNode', this.get('item'));
  },
  mouseLeave: function() {
    this.set('hoveredNode', null);
  },
  click: function() {
    this.set('hoveredNode', null);
    this.set('selectedNode', this.get('item'));
  }

});
