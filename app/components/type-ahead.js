import Ember from 'ember';


// straight from typeahead examples
function substringMatcher (strs) {

  return function (q, cb) {

    var matches, substrRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    strs.forEach(function(item) {
      if (substrRegex.test(item.value)) {
        // the typeahead jQuery plugin expects suggestions to a
        // JavaScript object, refer to typeahead docs for more info
        matches.push({
          value: item.value,
          obj: item
        });
      }
    });

    cb(matches);

  };

}


export default Ember.TextField.extend({


  // typeahead data to search
  data: null,


  // CSS class name postfix for typeahead dropdown
  name: null,


  // object that will be selected by typeahead interaction
  selectedItem: null,


  didInsertElement: function () {
    var cmp = this;

    // initialize typeahead
    this.$().typeahead({
      hint: true,
      highlight: true,
      minLength: 2
    }, {
      name: cmp.get('name'),
      displayKey: 'value',
      source: substringMatcher(cmp.get('data'))
    }).on('typeahead:selected', function(event , s) {
      cmp.set('selectedItem', s.obj);
    });

  }

});
