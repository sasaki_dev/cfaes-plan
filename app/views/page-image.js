import Ember from 'ember';

export default Ember.View.extend({

  templateName: 'page-image',

  classNames: ['img'],
  classNameBindings: ['isSmall'],

  isSmall: true,


  // tooltips,
  // for a clean UX they end up being a little more complicated
  // than they initially seemed
  updatePageNavTooltips: function () {

    if (this.get('controller.nextPage')) {
      this.$('a.next').tooltip({
        title: 'Go to next page',
        placement: 'left'
      });
    } else {
      this.$('a.next').tooltip('destroy');
    }

    if (this.get('controller.previousPage')) {
      this.$('a.previous').tooltip({
        title: 'Go to previous page',
        placement: 'right'
      });
    } else {
      this.$('a.previous').tooltip('destroy');
    }

  }.observes('controller.nextPage', 'controller.previousPage'),


  updateZoomBtn: function () {

    // toggle resize buttons
    if (this.get('isSmall')) {
      this.$('#resize-btn').tooltip('destroy');
      this.$('#resize-btn').tooltip({
        title: 'View larger image',
        placement: 'top'
      });
    } else {
      this.$('#resize-btn').tooltip('destroy');
      this.$('#resize-btn').tooltip({
        title: 'View smaller image',
        placement: 'top'
      });
    }

  }.observes('isSmall'),


  mouseEnter: function () {
    if (!this.get('isSmall')) {
      this.$('a').show();
    }
  },

  mouseLeave: function () {
    if (!this.get('isSmall')) {
      this.$('a').hide();
    }
  },


  didInsertElement: function () {

    this.$('#resize-btn').tooltip({
      title: 'View larger image',
      placement: 'top'
    });

  },


  actions: {
    expand: function () {
      this.toggleProperty('isSmall');
    }
  }

});
