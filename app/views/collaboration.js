import Ember from 'ember';

export default Ember.View.extend({

  classNames: ['chart'],
  templateName: 'collaboration',


  // graph elements
  nodes: null,
  links: null,


  // our typeahead component wants the label as 'value' property
  typeaheadData: null,
  updateTypeaheadData: function () {
    var data = this.get('nodes').map(function (n) {
      n.value = n.attributes.label;
      return n;
    });

    this.set('typeaheadData', data);

  }.observes('nodes'),


  // used when scaling graph into available canvas space
  graphPadding: 50,


  selectedNode: null,

  // make unique collaborations,
  // hack because of a minor data issue
  dedupedCollaborations: function() {
    var nodes, labels;

    nodes = this.get('selectedNode.collaborations.nodes') || [];
    nodes.removeObject(this.get('selectedNode'));
    labels = nodes.sortBy('color', 'attributes.label').mapBy('attributes.label').uniq();

    return labels.map(function(l) {
      return nodes.find(function(c) {
        return c.attributes.label === l;
      });
    });
  }.property('selectedNode'),


  // show collaborations on hovered or selected (clicked) node
  showCollaborations: function() {
    var node = this.get('selectedNode') || this.get('hoveredNode'),
        nodes = this.get('nodes'),
        links = this.get('links');

    // find collaborations
    if (node) {
      node.collaborations = {
        nodes: [],
        links: []
      };

      node.collaborations.links = links.filter(function(l) {
        return l.source === node.id || l.target === node.id;
      });

      node.collaborations.links.forEach(function(l) {
        node.collaborations.nodes.push(nodes[l.source]);
        node.collaborations.nodes.push(nodes[l.target]);
      });

      node.collaborations.nodes = node.collaborations.nodes.uniq();

      // highlight collaborations in graph
      d3.selectAll('.node')
        .each(function(n) {
          var isNeighbor = node.collaborations.nodes.contains(n),
              isCenter = n === node,
              strokeWidth = isNeighbor ? ( isCenter ? 3 : 2 ) : 1.4;

          d3.select(this)
            .attr('stroke-width', strokeWidth)
            .style('opacity', isNeighbor ? 1 : 0.2);
        });

      d3.selectAll('.link')
        .each(function(l) {
          var isNeighbor = node.collaborations.links.contains(l);

          d3.select(this)
            .style('stroke-width', isNeighbor ? 2 : 1)
            .style('opacity', isNeighbor ? 0.5 : 0.1);
        });
    }
  }.observes('selectedNode', 'hoveredNode'),


  hoveredNode: null,

  // labels hovered node
  labelNode: function() {
    var node = this.get('hoveredNode'),
        svgContainer = this.get('svgContainer'),
        label,
        offset = 2;

    if (node) {

      // add label
      label = svgContainer
        .append('g')
        .attr('class', 'graph-label');

      // white halo
      label
        .append('text')
        .attr('stroke', 'white')
        .attr('stroke-width', 3)
        .attr('x', node.x + offset)
        .attr('y', node.y - offset)
        .style('opacity', 0.9)
        .style('pointer-events', 'none')
        .style('font-style', 'oblique')
        .text(node.attributes.label);

      label
        .append('text')
        .attr('x', node.x + offset)
        .attr('y', node.y - offset)
        .style('pointer-events', 'none')
        .style('font-style', 'oblique')
        .text(node.attributes.label);

    } else {

      // remove label
      d3.select('.graph-label').remove();

    }

  }.observes('hoveredNode'),


  didInsertElement: function() {

    var view = this,
        data = this.get('controller.model.graph_data'),
        svgContainer, width, height,
        padding = this.get('graphPadding'),
        extents, scales,
        nodes, links,
        drawLine, circles, circleAttributes;


    svgContainer = d3.select('#graph-container')
      .append('svg');
    width = parseInt(svgContainer.style('width'));
    height = parseInt(svgContainer.style('height'));

    this.set('svgContainer', svgContainer);


    extents = {
      x: d3.extent(data.nodes.map(function(n) { return n.x; })),
      y: d3.extent(data.nodes.map(function(n) { return n.y; }))
    };

    scales = {
      x: d3.scale.linear()
        .domain([extents.x[0] - padding, extents.x[1] + padding])
        .range([0, width]),
      y: d3.scale.linear()
        .domain([extents.y[0] - padding, extents.y[1] + padding])
        .range([0, height])
    };


    // prepare data, scale node coordinates and link paths
    nodes = data.nodes.map(function(n) {
      n.x = scales.x(n.x);
      n.y = scales.y(n.y);
      return n;
    });
    links = data.edges.map(function(e) {
      e.path = [{
        x: nodes[e.source].x,
        y: nodes[e.source].y
      }, {
        x: nodes[e.target].x,
        y: nodes[e.target].y
      }];
      e.color = nodes[e.source].color || 'black';
      return e;
    });

    this.set('nodes', nodes);
    this.set('links', links);


    // draw graph lines
    drawLine = d3.svg.line()
      .x(function(d) { return d.x; })
      .y(function(d) { return d.y; })
      .interpolate('linear');

    svgContainer.selectAll('path')
      .data(links)
      .enter()
      .append('path')
      .attr('class', 'link')
      .attr('d', function(d) { return drawLine(d.path); })
      .attr('stroke', function(d) { return d.color; })
      .attr('stroke-width', 1)
      .attr('opacity', 0.5)
      .attr('fill', 'none');

    // draw graph nodes
    circles = svgContainer.selectAll('circle')
      .data(nodes)
      .enter()
      .append('circle');

    circleAttributes = circles
      .attr('class', 'node')
      .attr('cx', function(d) { return d.x; })
      .attr('cy', function(d) { return d.y; })
      .attr('r', function(d) { return d.size; })
      .attr('stroke', 'white')
      .attr('stroke-width', 1.4)
      .attr('fill', function(d) { return d.color; })
      .on('mouseover', nodeOver)
      .on('mouseout', nodeOut)
      .on('click', nodeClick);


    // interactivity
    svgContainer.on('click', function() { // 'reset node selection'
      resetChart();
      view.set('selectedNode', null);
    });

    function nodeOver(d) {
      view.set('hoveredNode', d);
    }

    function nodeOut() {
      view.set('hoveredNode', null);
      if (!view.get('selectedNode')) {
        resetChart();
      }
    }

    function nodeClick(d) {
      view.set('selectedNode', d);
      // event will be caught by SVG, which will reset the chart
      d3.event.stopPropagation();
    }


    // reset to original styles
    function resetChart() {
      d3.selectAll('.graph-label').remove();
      d3.selectAll('.node')
        .attr('stroke-width', 1.4)
        .style('opacity', 1);
      d3.selectAll('.link')
        .style('opacity', 0.5);
    }

  }

});
