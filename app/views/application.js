import Ember from 'ember';

export default Ember.View.extend({

  tooltipHtml: 'Click chapter icons ( <span class="glyphicon glyphicon-file"></span> ) to customize report',

  // only show tooltip for 'full report' button
  toggleTooltip: function () {

    if (this.get('controller.toPrint.length') > 0) {
      Ember.$('#download-link').tooltip('destroy');
    } else {
      Ember.$('#download-link').tooltip({
        html: true,
        title: this.get('tooltipHtml'),
        placement: 'bottom'
      });
    }

  }.observes('controller.toPrint.length'),

  didInsertElement: function() {

    // hide header on scroll
    Ember.$('#navbar').affix({
      offset: {
        top: 100,
        bottom: function () {
          return (this.bottom = Ember.$('.footer').outerHeight(true));
        }
      }
    });

    // initialize tooltip
    Ember.$('#download-link').tooltip({
      html: true,
      title: this.get('tooltipHtml'),
      placement: 'bottom'
    });

  }

});
