import Ember from 'ember';

function formatMarkdown(value) {
  return new Handlebars.SafeString(markdown.toHTML(value));
}

export {
  formatMarkdown
};

export default Ember.Handlebars.makeBoundHelper(formatMarkdown);
